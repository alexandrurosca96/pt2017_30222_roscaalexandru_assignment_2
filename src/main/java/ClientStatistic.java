

/**
 * Created by Rosca on 01.04.2017.
 */
public class ClientStatistic {
    Task client;
    int waitingTime;
    boolean isServed ;

    public ClientStatistic(Task client, int waitingTime){
        this.client = client;
        this.waitingTime = waitingTime;
        this.isServed = false;
    }
    public ClientStatistic(){}

    public String getName(){
        return client.getNameClient();
    }
    public int getWaitingTime(){
        return waitingTime;
    }
    public int getProcessingTime(){
        return client.getProcessingTime();
    }

    public boolean getIsServed(){
        return isServed;
    }
    public void setIsServed(){
        this.isServed = true;
    }
    public void setWaitingTime(int var){
        waitingTime = var;
    }
    public void setClient(Task client){
        this.client = client;
    }
    public Task getClient(){
        return client;
    }

}
