
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Rosca on 29.03.2017.
 */
public class Scheduler {

    private ArrayList<Server> servers = new ArrayList<Server>();
    private int maxNoServers;
    private Statistics statistics;
    private ArrayList<Thread> threads = new ArrayList<Thread>();
    public Thread getThread(int index){
        return threads.get(index);
    }
    private SimulationFrame view;

    public Scheduler(int maxNoServers, Statistics statistics, SimulationFrame view){
        this.view = view;
        this.statistics = statistics;
        this.maxNoServers = maxNoServers;
    }

    public void startServers(){
        for(int i = 0; i < maxNoServers; i++){
            Server serv = new Server("Server" + i, statistics, view);
            servers.add(serv);
            threads.add(new Thread(serv));
            Thread t = new Thread(serv);
            t.start();
        }
    }
    public void setMaxNoServers(int noOfServers){
        this.maxNoServers = noOfServers;
    }
    synchronized public void   dispatchTask(Task t){
        Date now = new Date();
        String format = new SimpleDateFormat("HH:mm:ss.SS", Locale.getDefault()).format(now);
        Collections.sort(servers, new Comparator<Server>() {
            public int compare(Server o1, Server o2) {
                return o1.getWaitingPeriod() - o2.getWaitingPeriod();
            }
        });

        view.setTextAfisare ("\n[" + format + "]" + t.getNameClient() + " just put in " + servers.get(0).getServerName()+ "\n");
        servers.get(0).addTask(t);
    }


    public List<Server> getServers(){
        return servers;
    }

    public void startThreadNo(int number){
        Thread t = new Thread(servers.get(number));
        t.start();

    }

    public Server getServer(int index){
        return servers.get(index);
    }

    public Statistics getStatistics(){
        return servers.get(0).getStatistics();
    }
}
