import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Rosca on 29.03.2017.
 */
public class Server implements  Runnable{
    private BlockingQueue<Task> tasks = new LinkedBlockingQueue<Task>();
    private AtomicInteger waitingPeriod ;
    private String name;
    private Statistics statistics;
    private SimulationFrame view;
    int timeLimit ;

    public Statistics getStatistics(){
        return statistics;
    }
    public String getServerName(){
        return name;
    }

    public Server(String name, Statistics statistics, SimulationFrame view){
        //initialize
        this.view = view;
        this.statistics = statistics;
        this.name = name;
        waitingPeriod = new AtomicInteger(0);
    }

    public synchronized void addTask(Task newTask)  {
        tasks.offer(newTask);
        waitingPeriod.addAndGet(newTask.getProcessingTime());
        statistics.addStatistics(getServerName(),newTask,getWaitingPeriod());
    }
    public void run(){
        long startTime = System.currentTimeMillis();
        int timeLimit = Integer.parseInt(view.getTimeLimit());
        while(System.currentTimeMillis() - startTime < (timeLimit ) * 1000){
            Date now = new Date();
            if(!tasks.isEmpty()){
                Task task = tasks.poll();

                String format = new SimpleDateFormat("HH:mm:ss.SS", Locale.getDefault()).format(now);
                view.setTextAfisare("\n[" + format + "]" +task.getNameClient() +
                        " started to be served at " + getServerName() +" and he has to wait " + task.getProcessingTime() + " sec more . . .\n");


                try {
                    Thread.sleep(task.getProcessingTime() * 1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                waitingPeriod.addAndGet( 0 - task.getProcessingTime());
                Date now1 = new Date();
                String format1 = new SimpleDateFormat("HH:mm:ss.SS", Locale.getDefault()).format(now1);
                if(System.currentTimeMillis() - startTime < (timeLimit + 0.5)  * 1000) {
                    view.setTextAfisare("\n[" + format1 + "]" + task.getNameClient() + " has been served at " + getServerName() + "...\n");
                    statistics.setServed(name, task);
                }else{
                    view.setTextAfisare("\n[" + format1 + "]" + task.getNameClient() + "has not been completely served because " + getServerName() + " was closed during serving... \n");
                }

            }
        }

    }


    public int getWaitingPeriod(){
        return waitingPeriod.intValue();
    }

    public Statistics getStatics(){
        return statistics;
    }

}

