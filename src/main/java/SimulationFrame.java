

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.ImageObserver;

/**
 * Created by Rosca on 29.03.2017.
 */
public class SimulationFrame {
    private static final long serialVersionUID = 1L;
    private int WIDTH = 600, HEIGHT = 600;

    JLabel labelTimeLimit = new JLabel("Time limit:");
    JLabel labelNoOfServers = new JLabel("Number of servers:");
    JLabel labelMinProcessingTime = new JLabel("Min Processing time:");
    JLabel labelMaxProcessingTime = new JLabel("Max Processing time:");
    JLabel labelMinTimeBetweenClients = new JLabel("Min time between clinets:");
    JLabel labelMaxTimeBetweenClients = new JLabel("Max time between clinets:");

    JTextField textTimeLimit = new JTextField();
    JTextField textNoOfServers = new JTextField();
    JTextField textMinProcessingTime = new JTextField();
    JTextField textMaxProcessingTime = new JTextField();
    JTextField textMinTimeBetweenClients = new JTextField();
    JTextField textMaxTimeBetweenClients = new JTextField();


    JTextArea textArea = new JTextArea(150,300);
    JScrollPane scroll = new JScrollPane();
    JFrame frame = new JFrame();
    JButton buttonStart = new JButton("Start");

    public SimulationFrame(){

        frame.setVisible(true);
        frame.setSize(WIDTH, HEIGHT);
        frame.setLayout(null);
        frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);

        JPanel panelInsert = new JPanel();
        JPanel panelAfisare = new JPanel();

        // panelInsert.setBackground(Color.LIGHT_GRAY);
        panelInsert.setVisible(true);
        panelInsert.setLayout(null);

        panelAfisare.setVisible(true);
        panelAfisare.setLayout(null);




        //adding in panelInsert
        panelInsert.add(labelTimeLimit);
        labelTimeLimit.setBounds(0,0, 150, 20);
        panelInsert.add(textTimeLimit);
        textTimeLimit.setBounds(160,0,30,20);

        panelInsert.add(labelNoOfServers);
        labelNoOfServers.setBounds(0,30, 150, 20);
        panelInsert.add(textNoOfServers);
        textNoOfServers.setBounds(160,30,30,20);


        panelInsert.add(labelMinProcessingTime);
        labelMinProcessingTime.setBounds(0,60, 150, 20);
        panelInsert.add(textMinProcessingTime);
        textMinProcessingTime.setBounds(160,60,30,20);

        panelInsert.add(labelMaxProcessingTime);
        labelMaxProcessingTime.setBounds(0,90, 150, 20);
        panelInsert.add(textMaxProcessingTime);
        textMaxProcessingTime.setBounds(160,90,30,20);

        panelInsert.add(labelMinTimeBetweenClients);
        labelMinTimeBetweenClients.setBounds(0,120, 150, 20);
        panelInsert.add(textMinTimeBetweenClients);
        textMinTimeBetweenClients.setBounds(160,120,30,20);

        panelInsert.add(labelMaxTimeBetweenClients);
        labelMaxTimeBetweenClients.setBounds(0,150, 150, 20);
        panelInsert.add(textMaxTimeBetweenClients);
        textMaxTimeBetweenClients.setBounds(160 ,150,30,20);

        panelInsert.add(buttonStart);
        buttonStart.setBounds(70,175,80,45);

        scroll.setBounds(16,0,WIDTH - 50,HEIGHT - 250);
        //scroll.createVerticalScrollBar(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS = 32);
        scroll.setVisible(true);
        //adding panelAfisare
        panelAfisare.add(textArea);

        textArea.setBounds(18,0, WIDTH - 40, HEIGHT-220);

        // scroll.setBorder(BorderFactory.createTitledBorder("Mess"));
        scroll.setViewportView(textArea);
        panelAfisare.add(scroll);


        frame.add(panelInsert);
        frame.add(panelAfisare);
        panelAfisare.setBounds(0, 230, WIDTH, HEIGHT- 220);
        panelInsert.setBounds(200,0,200,220);
    }
    public void showDialogMessage(String string){
        JOptionPane.showMessageDialog(frame, string);

    }

    public void setTextAfisare(String string){
        textArea.append(string);
    }

    public String getServers(){
        return textNoOfServers.getText();
    }

    public String getTimeLimit(){
        return textTimeLimit.getText();
    }

    public String getMinProcTime(){
        return textMinProcessingTime.getText();
    }
    public String getMaxProcTime(){
        return textMaxProcessingTime.getText();
    }

    public String getMinTimeBetweenClients(){
        return textMinTimeBetweenClients.getText();
    }
    public String getMaxTimeBetweenClients(){
        return textMaxTimeBetweenClients.getText();
    }
    public void addActionButtonStart(ActionListener listenerForStartButton){
        buttonStart.addActionListener(listenerForStartButton);
    }


    public void displayData(Task[][] tasks){
        /**
         * remove all previous elements from panel and revalidate panel
         * add a JScrollPane with a JList containing tasks
         * add scrollPane to panel
         * repaint and revalidate panel
         */

    }
}
