
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by Rosca on 29.03.2017.
 */
public class SimulationManager implements  Runnable {
    private int timeLimit ;
    private int maxProcessingTime ;
    private int minProcessingTime ;
    private int numberOfServers ;
    private int minTimeBetweenClients ;
    private int maxTimeBetweenClients ;
    private static int idClient = 0;
    private static int timeArriveClient = 0;
    private Scheduler scheduler;
    private SimulationFrame view;
    private Statistics statistics;
    private Thread threadGenerateClients;
    private Random random = new Random();
    ClientStatistic clientStatistic;


    public SimulationManager(Statistics statistics ,  SimulationFrame view ){
        this.view = view;
        this.statistics = statistics;
        scheduler = new Scheduler(numberOfServers, statistics, view);
        this.view.addActionButtonStart(new StartListener());
    }

    private Task generateClient(){
        int procTime = random.nextInt(maxProcessingTime - minProcessingTime) + minProcessingTime;
        if(procTime == 0){
            procTime = 1;
        }
        Task newClient = new Task(timeArriveClient, procTime, "Client " + idClient);
        idClient++;
        return newClient;
    }

    public void run() {
        long startTime = System.currentTimeMillis();
        scheduler.setMaxNoServers(numberOfServers);
        scheduler.startServers();
        while(System.currentTimeMillis() - startTime < timeLimit*1000){
            Task client = generateClient();
            int delayBetweenClients = minTimeBetweenClients + random.nextInt(maxTimeBetweenClients-minTimeBetweenClients);
            timeArriveClient += delayBetweenClients;

            Date now = new Date();
            String format = new SimpleDateFormat("HH:mm:ss.SS", Locale.getDefault()).format(now);

            view.setTextAfisare("\n[" + format + "]" + client.getNameClient() + " just arrived in shop!\n");
            scheduler.dispatchTask(client);

            try {
                Thread.sleep(delayBetweenClients * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        view.showDialogMessage("Waiting for results...");
        try {
            Thread.sleep(maxProcessingTime * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        showResults();
    }

    public void showResults(){
        TreeMap<String, ArrayList<ClientStatistic>> finalResultsStatistics = scheduler.getStatistics().getResults();
        for(Map.Entry<String, ArrayList<ClientStatistic>> entry: finalResultsStatistics.entrySet()) {
            if (entry.getKey() != null) {
                view.setTextAfisare("\n----" + entry.getKey() + "-----\n");
                int serviceTime = 0, waitingTime = 0, count = 0;
                boolean empty =false;
                clientStatistic = statistics.peakHour(entry.getKey());
                for (ClientStatistic clients : entry.getValue()) {
                    if(clients.getIsServed()) {
                        count++;
                        serviceTime += clients.getProcessingTime();
                        waitingTime += clients.getWaitingTime();
                        view.setTextAfisare(clients.getName() + " " + "served\n");
                    }else{
                        view.setTextAfisare(clients.getName() + " not served\n");
                        empty = true;
                    }
                }
                if(count!=0) {
                    view.setTextAfisare("\nTotal processing time: " + ((empty ? timeLimit : serviceTime)) + " sec");
                    view.setTextAfisare("\nTotal empty time: " + ((empty ? 0 : (timeLimit - serviceTime)) + " sec"));
                    view.setTextAfisare("\nAvarage processing time: " + ((float) serviceTime / count) + " sec");
                    view.setTextAfisare("\nAvarage waiting time: " + (float) (waitingTime / count) + " sec");
                    view.setTextAfisare("\nPeak hour was at: " + clientStatistic.getClient().getArrivalTime() + " sec for " + clientStatistic.getClient().getNameClient() + " with " + clientStatistic.getWaitingTime() + " sec to wait");
                    view.setTextAfisare("\n----------------");
                }

            }
        }

    }

    public class StartListener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            try{
                setTimeLimit();
                setNumberOfServers();
                setMaxProcessingTime();
                setMinProcessingTime();
                setMinTimeBetweenClients();
                setMaxTimeBetweenClients();
                startThread();
            }catch (Exception er){
                view.setTextAfisare("Enter valid values!!");
            }
        }
    }

    public void startThread(){
        if (threadGenerateClients == null){
            threadGenerateClients = new Thread(this);
            threadGenerateClients.start();
        }
    }

    public void setTimeLimit(){
        timeLimit = Integer.parseInt(view.getTimeLimit());
    }

    public void setNumberOfServers(){
        numberOfServers = Integer.parseInt(view.getServers());
    }
    public void setMaxProcessingTime(){
        maxProcessingTime = Integer.parseInt(view.getMaxProcTime());
    }
    public void setMinProcessingTime(){
        minProcessingTime = Integer.parseInt(view.getMinProcTime());
    }
    public void setMinTimeBetweenClients(){
        minTimeBetweenClients = Integer.parseInt(view.getMinTimeBetweenClients());
    }
    public void setMaxTimeBetweenClients(){
        maxTimeBetweenClients = Integer.parseInt((view.getMaxTimeBetweenClients()));
    }

    public static void main(String [] args)  {
        Statistics statistics = new Statistics();
        SimulationFrame view = new SimulationFrame();
        SimulationManager gen = new SimulationManager(statistics, view);

    }
}
