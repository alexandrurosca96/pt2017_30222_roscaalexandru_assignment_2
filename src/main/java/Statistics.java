
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class Statistics {
    TreeMap<String, ArrayList<ClientStatistic>> statistics = new TreeMap<String, ArrayList<ClientStatistic>>();
    ArrayList<ClientStatistic> clientStatistics1 = new ArrayList<ClientStatistic>();
    ArrayList<ClientStatistic> clientStatistics2 = new ArrayList<ClientStatistic>();
    ArrayList<ClientStatistic> clientStatistics3 = new ArrayList<ClientStatistic>();


    public int getWaitingTimeForClient(Task client) {
        int wait = 0;
        for (Map.Entry<String, ArrayList<ClientStatistic>> entry : statistics.entrySet()) {
            for (ClientStatistic clientStatistic : entry.getValue()) {
                if(clientStatistic.getName().equals(client.getNameClient())){
                    wait = clientStatistic.getWaitingTime();
                }
            }
        }
        return wait;
    }

    public  ClientStatistic peakHour(String nameServer){
        ClientStatistic clientStatisticMax = new ClientStatistic();
        for(Map.Entry<String, ArrayList<ClientStatistic>> entry: statistics.entrySet()){
            if(entry.getKey().equals(nameServer)){
                clientStatisticMax.setWaitingTime(0);
                for(ClientStatistic clientStatistic: entry.getValue()){
                    if(clientStatistic.getWaitingTime() > clientStatisticMax.getWaitingTime() && clientStatistic.getIsServed()){
                        clientStatisticMax.setWaitingTime(clientStatistic.getWaitingTime());
                        clientStatisticMax.setClient(clientStatistic.getClient());
                    }
                }
            }
        }

        return clientStatisticMax;
    }

    public synchronized void addStatistics(String nameServer, Task client, int waitingTime){
        ClientStatistic newClient = new ClientStatistic(client, waitingTime);
        if(nameServer.equals("Server0")) {
            clientStatistics1.add(newClient);
            statistics.put(nameServer, clientStatistics1);
        }
        if(nameServer.equals("Server1")) {
            clientStatistics2.add(newClient);
            statistics.put(nameServer, clientStatistics2);
        }else {
            clientStatistics3.add(newClient);
            statistics.put(nameServer, clientStatistics3);
        }
    }

    public synchronized void setServed(String nameServer, Task client) {
        if (nameServer.equals("Server0")) {
            for (ClientStatistic cl : clientStatistics1) {
                if (cl.getName().equals(client.getNameClient())) {
                    cl.setIsServed();
                }
            }
        }

        if (nameServer.equals("Server1")) {
            for (ClientStatistic cl : clientStatistics2) {
                if (cl.getName().equals(client.getNameClient())) {
                    cl.setIsServed();
                }
            }
        }
        if (nameServer.equals("Server2")) {
            for (ClientStatistic cl : clientStatistics3) {
                if (cl.getName().equals(client.getNameClient())) {
                    cl.setIsServed();
                }
            }
        }
    }


    public TreeMap<String, ArrayList<ClientStatistic>> getResults(){
        return statistics;
    }







}

