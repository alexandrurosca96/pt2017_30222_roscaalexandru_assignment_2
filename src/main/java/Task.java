

public class Task {
    private int arrivalTime;
    private int processingTime;
    private String nameClient;
    private int finishTime ; //+ waitingPeriodOnServer
    public Task(int arrivalTime, int processingTime, String nameClient){
        this.arrivalTime = arrivalTime;
        this.processingTime = processingTime;
        this.nameClient = nameClient;

    }
    @Override
    public String toString() {
        String s  = nameClient + "is served in " + getProcessingTime() + " seconds";
        return s;
    }

    public int getProcessingTime(){
        return processingTime;
    }
    public void setProcessingTime(int newVal){
        this.processingTime = newVal;
    }

    public int getArrivalTime(){
        return arrivalTime;
    }

    public void setArrivalTime(int val){
        this.arrivalTime = val;
    }
    public void setNameClient(String name){
        this.nameClient = nameClient;
    }
    public String getNameClient(){
        return nameClient;
    }
    public void setFinishTime(int waitingPeriodServer){
        finishTime = arrivalTime + processingTime + waitingPeriodServer;
    }
    public int getFinishTime(){
        return finishTime;
    }
}
